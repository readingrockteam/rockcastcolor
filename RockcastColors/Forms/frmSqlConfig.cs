﻿using System;
using System.Windows.Forms;

namespace RockcastColors.Forms
{
    public partial class frmSqlConfig : Form
    {
        public bool Saved { get; private set; }

        public string Title { get; set; }
        public frmSqlConfig()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Text = Title;
            btnOk.Enabled = false;
            Saved = false;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            var connStr = string.Format("Data Source={0};Initial Catalog={1};Trusted_Connection=yes;", txtServer.Text,
                "'" + txtDatabase.Text + "'");
            try
            {
                var helper = new SqlHelper(connStr);
                if (helper.IsConnected)
                    MessageBox.Show("Test connection succeeded.", "Test Connection", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                btnOk.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var connStr = string.Format("Data Source={0};Initial Catalog={1};Trusted_Connection=yes;", txtServer.Text, '"' + txtDatabase.Text + '"');
            try
            {
                var helper = new SqlHelper(connStr);
                if (helper.IsConnected)
                {
                    var appSetting = new AppSetting();
                    appSetting.SaveConnectionString(Title, connStr);
                }

                Saved = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            Close();
        }

        private void txtServer_TextChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = false;
        }

        private void txtDatabase_TextChanged(object sender, EventArgs e)
        {
            btnOk.Enabled = false;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
