﻿using System;
using System.Configuration;
namespace RockcastColors
{
    public class AppSetting
    {
        private readonly Configuration _config;

        public AppSetting()
        {
            _config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        public string GetConnectionString(string key)
        {
            return _config.ConnectionStrings.ConnectionStrings[key].ConnectionString;
        }

        public void SaveConnectionString(string key, string value)
        {
            _config.ConnectionStrings.ConnectionStrings[key].ConnectionString = "metadata=res://*/PSEntities.csdl|res://*/PSEntities.ssdl|res://*/PSEntities.msl;provider=System.Data.SqlClient;provider connection string=\'" + value + "MultipleActiveResultSets=true;App=EntityFramework\'";
            _config.ConnectionStrings.ConnectionStrings[key].ProviderName = "System.Data.EntityClient";
            try
            {
                _config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
