﻿
using System.Collections.Generic;

namespace RockcastColors.Classes
{
    public class RockcastColor
    {
        public string Color { get; set; }
        public string ColorCode { get; set; }
        public int NumColors { get; set; }
        public string LogoPath { get; set; }
    }

    public class RockCastColors
    {
        private List<RockcastColor> _list;

        public RockCastColors()
        {
            _list = new List<RockcastColor>();
        }
    }
}
