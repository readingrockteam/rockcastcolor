﻿using System;
using System.Reflection;
using System.Windows.Forms;
using RockcastColors.Dialogs;
using RockcastColors.Forms;

namespace RockcastColors
{
    public partial class Form1 : Form
    {
        #region < Members >

        private string _catalog = string.Empty;
        #endregion
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dlg = new AboutBox1();
            dlg.ShowDialog(this);
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            using (var data = new Packing_SolutionEntities())
            {
                dataGridView1.DataSource = data.spCopyColorCodestoRockcastColors();
            }

            Cursor.Current = DefaultCursor;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var appSetting = new AppSetting();
            var connStr = appSetting.GetConnectionString("Packing_SolutionEntities");

            var connArr = connStr.Split(';');
            GetSource(connArr, out _catalog);
            Text = "99 Rockcast Colors Refresh v. " + Assembly.GetExecutingAssembly().GetName().Version + "   PS: " + _catalog.Replace("'", "");
        }

        private void GetSource(string[] ConnArr, out string Source)
        {
            Source = string.Empty;
            foreach (var el in ConnArr)
            {
                if (el.ToLower().Contains("catalog"))
                    Source = el.Split('=')[1];
            }
        }

        private void setPSDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var obj = new frmSqlConfig { Title = "Packing_SolutionEntities" };
            obj.ShowDialog(this);
            if (obj.Saved)
            {
                var appSetting = new AppSetting();
                var connStr = appSetting.GetConnectionString("Packing_SolutionEntities");

                var connArr = connStr.Split(';');
                GetSource(connArr, out _catalog);
                Text = "99 Rockcast Colors Refresh v. " + Assembly.GetExecutingAssembly().GetName().Version + "   PS: " + _catalog.Replace("'", "");
            }
        }

        private void seePSConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var appSetting = new AppSetting();
            var connStr = appSetting.GetConnectionString("Packing_SolutionEntities");
            MessageBox.Show(connStr);
        }
    }
}
